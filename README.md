# Java Microservices
Microservices Java sample projects using

- Spring Boot
- Spring Cloud

Services Launch Order:

1. Netflix Eureka Naming Server Application
2. Currency Exchange Service Application
3. Currency Conversion Service Application
4. Zuul API Gateway
5. Zipkin 

		Launch Zipkin Server

		1. Set RABBIT_URI= amqp://localhost
		2. java -jar zipkin....
		
		
		
		Hot Properties Configuration Refresh
		http://localhost:8080/actuator/bus-refresh